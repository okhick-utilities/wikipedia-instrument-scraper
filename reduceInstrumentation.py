import json

with open('list.json', encoding='utf8') as listJSON:
    instList = json.load(listJSON)

filteredList = []

for section in instList:
    for instrument in instList[section]['instruments']:
        # filteredList.append(instrument['Instrument'])
        filteredList.append(instrument)

with open('listReduced.json', 'w', encoding='utf8') as json_file:
    json.dump(filteredList, json_file, indent=2, ensure_ascii=False)
