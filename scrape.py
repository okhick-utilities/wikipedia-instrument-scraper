from bs4 import BeautifulSoup
from urllib.request import urlopen
import json

# =======================================================
# =======================================================
# =======================================================

URL = "https://en.wikipedia.org/wiki/List_of_musical_instruments"
PAGE = urlopen(URL)
HTML = PAGE.read().decode("utf-8")
SOUP = BeautifulSoup(HTML, "html.parser")

# These are the section ids of the instrument list
section_ids = [
    'Percussion_instruments',
    'Membranophones',
    'Wind_instruments_(aerophones)',
    'Stringed_instruments_(chordophones)'
]

# =======================================================
# =======================================================
# =======================================================


def filterSections(section):
    sectionWithId = section.find(
        attrs={"id": section_ids, "class": "mw-headline"})
    return True if sectionWithId != None else False


def getSectionHeaders(sectionContent):
    listHeaders = sectionContent['table'].find_all('th')
    return list(map(lambda header: header.get_text().strip('\n'), listHeaders))


def getInstrumentData(sectionContent, headerText):
    listRows = sectionContent['table'].find_all('tr')
    instruments = []

    for row in listRows:
        instrument = {}
        columns = row.find_all('td')

        if (len(columns) != 0):
            for index, header in enumerate(headerText):
                instrument[header] = columns[index].get_text().strip('\n')

            instruments.append(instrument)

    return instruments


def returnToArray(string):
    return True


# =======================================================
# =======================================================
# =======================================================

if __name__ == "__main__":
    # get all possible sections and filter out sections we don't need
    sectionsAll = SOUP.select("h2,h3")
    SECTIONS = list(filter(filterSections, sectionsAll))

    # get the raw html sections
    instrumentLists = {}
    for index, section in enumerate(SECTIONS):
        section_id = section_ids[index]

        sectionWithId = section.find(
            attrs={"id": section_id, "class": "mw-headline"})
        instrumentLists[section_id] = {}
        instrumentLists[section_id]['header'] = sectionWithId
        instrumentLists[section_id]['table'] = section.find_next('table')

    # parse the html tables into json
    instrumentListsParsed = {}
    for section, sectionContent in instrumentLists.items():
        instrumentListsParsed[section] = {}

        # get all the headers used in this section
        instrumentListsParsed[section]['headerText'] = getSectionHeaders(
            sectionContent)

        # get the instrumentation lists
        instrumentListsParsed[section]['instruments'] = getInstrumentData(
            sectionContent, instrumentListsParsed[section]['headerText'])

    # turn listrument lists delimited by a \n into an array
    for section in instrumentListsParsed:
        for index, instrument in enumerate(instrumentListsParsed[section]['instruments']):
            instrumentString = instrument['Instrument']
            hasReturn = instrumentString.find('\n')
            if (hasReturn > 0):
                instrumentArray = instrumentString.split('\n')
                instrumentListsParsed[section]['instruments'][index]['Instrument'] = instrumentArray

    # write to file
    with open('list.json', 'w', encoding='utf8') as json_file:
        json.dump(instrumentListsParsed, json_file,
                  indent=2, ensure_ascii=False)
